<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Error\InvalidImageDescriptionException;
use AppBundle\Service\Db\UserDb;
use AppBundle\Service\Image;
use AppBundle\Utils\ImageDescription;
use HttpInvalidParamException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

class DefaultController extends Controller
{
    /**
     * @Route("/{hash}/", name="image")
     */
    public function imageAction(Request $request)
    {
        $user = $this->getUserProfile($request);

        $imageDescription = $this->getImageDescription($request);

        /** @var UserDb $userRepository */
        $userRepository = $this->get('app.repository.user');
        $userRating =  $userRepository->getAverageRating($user);
        $imageDescription->text = $userRating . '%';

        /** @var Image $image */
        $image = $this->get('app.image');

        try {
            $image = $image->getImage($imageDescription);
        }
        catch (InvalidImageDescriptionException $exception) {
            throw new NotAcceptableHttpException($exception->getMessage());
        }

        return $this->generateResponseFromImage($image);
    }

    protected function getImageDescription(Request $request) {
        $imageDescription = new ImageDescription();
        $imageDescription->height = $request->get('height', 100);
        $imageDescription->width = $request->get('width', 100);
        $imageDescription->textColor = $request->get('text-color', 'FFFFFF');
        $imageDescription->backgroundColor = $request->get('background-color', '000000');

        return $imageDescription;
    }

    protected function getUserProfile(Request $request) {
        $hash = $request->get('hash');

        /** @var User $user */
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(['hash' => $hash]);
        if (!$user) {
            throw new NotFoundHttpException('No user for this hash');
        }

        if (!$user->getActive()) {
            throw new NotFoundHttpException('User ' . $user->getId() . ' is not active');
        }

        return $user;
    }

    protected function generateResponseFromImage($image) {
        ob_start();
        imagepng($image);
        $str = ob_get_clean();

        $response = new Response($str, 200);
        $response->headers->set('Content-Type', 'image/png');

        $response->setPublic();
        $cacheAge = 60 * 20;
        $response->setMaxAge($cacheAge);
        $response->setSharedMaxAge($cacheAge);

        return $response;
    }
}
