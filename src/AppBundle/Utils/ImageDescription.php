<?php

namespace AppBundle\Utils;


class ImageDescription
{

    public $width = 100;
    public $height = 100;
    public $backgroundColor = '000000';
    public $textColor = 'FFFFFF';
    public $text = null;

} 