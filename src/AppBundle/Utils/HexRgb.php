<?php

namespace AppBundle\Utils;


class HexRgb {

    public function hexrgb ($hexstr)
    {
        $int = hexdec($hexstr);

        return array("red" => 0xFF & ($int >> 0x10),
            "green" => 0xFF & ($int >> 0x8),
            "blue" => 0xFF & $int);
    }
} 