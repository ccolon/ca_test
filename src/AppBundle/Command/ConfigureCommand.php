<?php


namespace AppBundle\Command;

use CCyrille\CommonBundle\RethinkDb\Rethink;
use CCyrille\FixturesBundle\DataFixtures\SBO\ProductFixture;
use Guzzle\Http\Exception\ServerErrorResponseException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Guzzle\Http\Client;

class ConfigureCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        parent::configure();

        $this
            ->setName('dev:configure')
            ->setDescription('Configure the symfony project (dump DB, clear caches & logs, ...)')
            ->addOption('rebuild', null, InputOption::VALUE_NONE, 'To rebuild your schema')
            ->addOption('dont-notify', null, InputOption::VALUE_NONE, 'No not produce a user notification')
            ->addOption(
                'bypass-db-check',
                null,
                InputOption::VALUE_NONE,
                'CAUTION you can drop a production database like that'
            );
    }

    /**
     * @var OutputInterface
     */
    protected $output = null;
    protected $check_bd = true;

    /**
     * Executes the current command.
     *
     * @param InputInterface $input An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     *
     * @return integer 0 if everything went fine, or an error code
     *
     * @throws \LogicException When this abstract class is not implemented
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        if (!$input->getOption('bypass-db-check')) {
            $this->checkDatabaseName();
        } else {
            $this->check_bd = false;
        }

        $this->cleanLogs();

        if ($input->getOption('rebuild')) {
            $this->rebuildFixtures();
            $this->exportFixturesToFile();
        } else {
            $this->importFixturesFromFile();
        }
    }

    protected function getConsolePath()
    {
        return $this->getContainer()->get('kernel')->getRootDir() . '/../bin/console';
    }


    public function bypassDBCheck()
    {
        $this->check_bd = false;
    }

    protected function execution($command)
    {
        exec($command, $output);
        foreach ($output as $line) {
            $this->output->writeLn('<info>' . $line . '</info>');
        }
    }

    protected function cleanLogs()
    {
        $this->output->writeLn('<info>Logs cleaned</info>');
    }

    protected function fixRights()
    {
        $this->output->writeLn('<info>Rights fixed</info>');
        $this->execution('chmod 777 -R app/cache app/logs');
    }

    protected function notifyUser()
    {
        $this->execution('notify-send "Fixtures load" "RMS"');
    }

    protected function rebuildFixtures()
    {

        $flags = ' --verbose';

        $this->execution('php ' . $this->getConsolePath() . ' doctrine:database:drop --force' . $flags);
        $this->execution('php ' . $this->getConsolePath() . ' doctrine:database:create' . $flags);
        $this->execution('php ' . $this->getConsolePath() . ' doctrine:schema:create' . $flags);
        $this->execution('php ' . $this->getConsolePath() . ' doctrine:fixtures:load --no-interaction --env=dev'.$flags);
    }

    protected function exportFixturesToFile()
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $root_dir = $this->getContainer()->get('kernel')->getRootDir();
        $param = $em->getConnection()->getParams();

        $file = $root_dir . '/cache/fixturedump.sql';

        $user = '-u ' . $param['user'];
        $password = '';
        if ($param['password'] != '') {
            $password = '-p' . $param['password'];
        }
        $database = $param['dbname'];

        $this->execution('mysqldump ' . $user . '  ' . $password . ' ' . $database . ' > ' . $file);
        $this->output->writeLn('<info>' . $database . ' database exported in ' . $file . '</info>');
    }

    protected function importFixturesFromFile()
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $root_dir = $this->getContainer()->get('kernel')->getRootDir();
        $param = $em->getConnection()->getParams();

        $file = $root_dir . '/cache/fixturedump.sql';

        if (!file_exists($file)) {
            throw new \Exception($file . ' does not exist. Please use the --rebuild first');
        }

        $user = '-u ' . $param['user'];
        $password = '';
        if ($param['password'] != '') {
            $password = '-p' . $param['password'];
        }
        $database = $param['dbname'];

        $this->execution('php ' . $this->getConsolePath() . ' doctrine:database:drop --force');
        $this->execution('php ' . $this->getConsolePath() . ' doctrine:database:create');
        $this->execution('mysql ' . $user . '  ' . $password . ' ' . $database . ' < ' . $file);
        $this->output->writeLn('<info>' . $database . ' database imported in ' . $file . '</info>');
    }

    protected function checkDatabaseName()
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        if ($em->getConnection()->getDatabase() != 'ca_test') {
            throw new \Exception('The dev:configure can run only if the database name is "ca_test"');
        }
    }



}
