<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Review;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ReviewsFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = json_decode(file_get_contents(__DIR__ . '/reviews.json'), true);

        foreach ($data as $jsonReview) {
            $userAdmin = new Review();
            $userAdmin->setRating($jsonReview['rating']);
            $userAdmin->setPublished($jsonReview['published']);

            /** @var User $user */
            $user = $this->getReference('user'. $jsonReview['user_id']);

            $userAdmin->setUser($user);

            $manager->persist($userAdmin);
            $this->addReference('review' . $jsonReview['id'], $userAdmin);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}