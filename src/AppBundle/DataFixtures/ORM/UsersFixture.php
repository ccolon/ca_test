<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class UsersFixture extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = json_decode(file_get_contents(__DIR__ . '/users.json'), true);

        foreach ($data as $jsonUser) {
            $userAdmin = new User();
            $userAdmin->setHash($jsonUser['hash']);
            $userAdmin->setActive($jsonUser['active']);

            $manager->persist($userAdmin);
            $this->addReference('user' . $jsonUser['id'], $userAdmin);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}