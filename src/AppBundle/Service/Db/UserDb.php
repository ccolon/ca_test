<?php

namespace AppBundle\Service\Db;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;

/**
 * UserRepository
 *
 */
class UserDb
{
    protected $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function getAverageRating(User $user) {
        $em = $this->em;

        $query = 'SELECT AVG(review.rating) from AppBundle\Entity\Review review where review.user = :userId and review.published = true';

        $query = $em->createQuery($query);
        $query->setParameter('userId', $user->getId());

        $result = $query->getResult();
        $result = $result[0][1];
        $result = ceil($result);
        return $result;
    }
}
