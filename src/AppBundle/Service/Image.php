<?php

namespace AppBundle\Service;


use AppBundle\Error\InvalidImageDescriptionException;
use AppBundle\Utils\HexRgb;
use AppBundle\Utils\ImageDescription;

class Image {

    public function getImage(ImageDescription $imageDescription) {
        $this->validateImageDescription($imageDescription);

        $image = imagecreatetruecolor($imageDescription->width, $imageDescription->height);
        if ( !$image ) {
            throw new \Exception('Cannot initialize new GD image stream');
        }
        $hexRgb = new HexRgb();

        $colors = $hexRgb->hexrgb($imageDescription->textColor);
        $textColor = imagecolorallocate($image, $colors['red'], $colors['green'], $colors['blue']);

        $colors = $hexRgb->hexrgb($imageDescription->backgroundColor);
        $backgroundColor = imagecolorallocate($image, $colors['red'], $colors['green'], $colors['blue']);

        imagefill($image,0,0,$backgroundColor);

        $this->imageCenteredString($image, 10, 0, $imageDescription->width, $imageDescription->height / 2,
            $imageDescription->text, $textColor);

        return $image;
    }

    protected function imageCenteredString ( &$img, $font, $xMin, $xMax, $y, $str, $col ) {
        $textWidth = imagefontwidth( $font ) * strlen( $str );
        $xLoc = ( $xMax - $xMin - $textWidth ) / 2 + $xMin + $font - $font / 2;
        imagestring( $img, $font, $xLoc, $y, $str, $col );
    }

    protected function validateImageDescription(ImageDescription $imageDescription) {
        if ($imageDescription->width > 500) {
            throw new InvalidImageDescriptionException('Width should be inferior at 500');
        }
        if ($imageDescription->width < 100) {
            throw new InvalidImageDescriptionException('Width should be superior at 100');
        }
        if ($imageDescription->height > 500) {
            throw new InvalidImageDescriptionException('Height should be inferior at 500');
        }
        if ($imageDescription->height < 100) {
            throw new InvalidImageDescriptionException('Height should be superior at 100');
        }
        if (!$imageDescription->text) {
            throw new InvalidImageDescriptionException('Text cannot be null');
        }
        // Add check for hexadecimal values
    }

} 