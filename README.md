ca_test
=======

Bootstrap the project:

 - clone
 - composer install
 - bin/console dev:configure --rebuild
 - bin/console server:run localhost:9568

Url example:
http://localhost:9568/sdf7sef84sfe84sef87/?height=100&width=100&text-color=0000FF&background-color=E2E1E2

Run Tests:
Command "codecept run acceptance ImageCept" in root dir

Command "phpunit" in root dir

Description of the task in Task.md