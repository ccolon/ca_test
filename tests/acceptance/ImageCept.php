<?php 
$I = new AcceptanceTester($scenario);

$I->wantTo('verify that an invalid hash in the url provide a 404');
$I->amOnPage('/45555444asd/');
$I->seeResponseCodeIs(404);
$I->see("No user for this hash");

$I->wantTo('verify that an inactive user provide a 404');
$I->amOnPage('/tyf7sef84sfe84sef53/');
$I->seeResponseCodeIs(404);
$I->see("User 3 is not active");

$I->wantTo('verify that an active user provide a valid image');
$I->amOnPage('/sdf7sef84sfe84sef87/');
$I->seeResponseCodeIs(200);

$I->wantTo('verify that an active user provide a valid image');
$I->amOnPage('/sdf7sef84sfe84sef87/?height=500&width=500&text-color=FFFFFF&background-color=000000');
$I->seeResponseCodeIs(200);

$I->wantTo('verify that an incorrect configuration for image provide a error');
$I->amOnPage('/sdf7sef84sfe84sef87/?height=500&width=1000&text-color=FFFFFF&background-color=000000');
$I->see("Width should be inferior at 500");
$I->seeResponseCodeIs(406);