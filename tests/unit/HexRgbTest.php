<?php


class HexRgbTest extends PHPUnit_Framework_TestCase
{
    public function testHexRgb()
    {
        $hexRgb = new \AppBundle\Utils\HexRgb();
        $colors = $hexRgb->hexrgb('FFFFFF');
        $this->assertEquals($colors, ['red' => 255, 'green' => 255, 'blue' => 255]);
    }

    public function testHexRgb2()
    {
        $hexRgb = new \AppBundle\Utils\HexRgb();
        $colors = $hexRgb->hexrgb('000000');
        $this->assertEquals($colors, ['red' => 0, 'green' => 0, 'blue' => 0]);
    }

    public function testHexRgb3()
    {
        $hexRgb = new \AppBundle\Utils\HexRgb();
        $colors = $hexRgb->hexrgb('EAEAEA');
        $this->assertEquals($colors, ['red' => 234, 'green' => 234, 'blue' => 234]);
    }
}