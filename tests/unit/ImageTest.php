<?php


class ImageDescriptionTest extends PHPUnit_Framework_TestCase
{
    public function testImageGeneration()
    {
        $image = new \AppBundle\Service\Image();

        $imageDescription = new \AppBundle\Utils\ImageDescription();
        $imageDescription->text = "Hello world";

        $imageDescription->height = 500;
        $imageDescription->width = 500;
        $imageDescription->textColor = 'FFFFFF';
        $imageDescription->backgroundColor = 'E2E1E2';
        $imageDescription->text = '100%';
        $imageGenerated = $image->getImage($imageDescription);

        ob_start();
        imagepng($imageGenerated);
        $str1 = ob_get_clean();

        $str2 = file_get_contents(__DIR__ . '/data/image1.png');

        $this->assertTrue($str1 == $str2);
    }


    /**
     * @expectedException \AppBundle\Error\InvalidImageDescriptionException
     */
    public function testImageDescriptionValidation()
    {
        $image = new \AppBundle\Service\Image();

        $imageDescription = new \AppBundle\Utils\ImageDescription();
        $imageDescription->text = "Hello world";

        $imageDescription->height = 25;
        $image->getImage($imageDescription);
    }

    /**
     * @expectedException \AppBundle\Error\InvalidImageDescriptionException
     */
    public function testImageDescriptionValidation2()
    {
        $image = new \AppBundle\Service\Image();

        $imageDescription = new \AppBundle\Utils\ImageDescription();
        $imageDescription->text = "Hello world";

        $imageDescription->height = 550;
        $image->getImage($imageDescription);
    }

    /**
     * @expectedException \AppBundle\Error\InvalidImageDescriptionException
     */
    public function testImageDescriptionValidation3()
    {
        $image = new \AppBundle\Service\Image();

        $imageDescription = new \AppBundle\Utils\ImageDescription();

        $imageDescription->text = null;
        $image->getImage($imageDescription);
    }
}