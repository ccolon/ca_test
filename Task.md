Let's build a small application which is capable of rendering png widgets (images) for users. The users should be stored in the database and have at least the following information:
some unique hash that can be used as a public identifier
some information about the status (can be active or inactive)
The app should have one public endpoint (per user) that can return image data and respects the semantics of http status codes. It should only return image data if the user is active (uses the state information mentioned above). The widget itself should have 4 dynamic attributes:
width (number in px between 100 and 500)
height (number in px between 100 and 500)
background-color (hex value, e.g. 000000 means black)
text-color (hex value)
Those attributes and the public identifier of the user should be included somehow in the route/path for the endpoint and thus used and validated by the app to generate a correct widget.

The user also has a relation to his reviews. A review itself only has two properties: average rating (0-100%) and a flag that indicates if the review is published or not. There can be thousands of reviews per user and millions in total.

The content of the widget itself is only one number: the average rating percentage of all published reviews of that user rounded to an integer number. Attached you find one example of a widget with 100*100 px and black background + white text.

Lets also keep performance in mind and as a first step set some caching headers on the response to tell clients that they can/should cache the widget for up to 20 minutes. Feel free to also include some sort of server side caching (not mandatory though for this task). The only requirement is that the image is updated at least every 20 minutes.
Some further requirements:
Symfony 3.0.*
MySQL or Postgres database
compliant with PSR-1/2
Tests ;)
